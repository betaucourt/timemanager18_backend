# Requirements

PhP >= 7.3, Apache/httpd, Mysql, (Optional) phpMyAdmin

# install dependencies

`composer install`

# Check ./.env

If it does not exist : `touch .env`
"DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name"

# Creating Database

`php bin/console doctrine:database:create`

# Updating Database Schema

`php bin/console doctrine:schema:update --force`

# generating client oauth (Information available in mysql/PhPMyAdmin in "client" )

`php bin/console fos:oauth-server:create-client --grant-type="password"`


# Run server

`php bin/console server:run`
Default url: 127.0.0.1:8000

## List
All routes: /api/doc

# Entities
auth_code / access_token / client : Belong to Oauth service

user (id, username, email)
workingTimes (id, start, end, user)
Clocks (id, time, status, user)
