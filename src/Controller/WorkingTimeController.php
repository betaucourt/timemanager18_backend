<?php

namespace App\Controller;

use App\Entity\Workingtimes;
use DateTime;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use phpDocumentor\Reflection\Types\Integer;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Form\UserType;

use FOS\RestBundle\Request\ParamFetcherInterface;
/**
 * User controller.
 *
 * @Route("/api")
 */
class WorkingTimeController extends FOSRestController
{

    /**
     * Create a new workingTime for a user.
     *
     * This is allowed to all
     *
     * @Rest\Post("/workingtimes/{userId}", name="new_workingtime")
     * @Rest\View()
     */
    public function newWorkingTime(Request $request, $userId)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($userId);
        $workingTime = json_decode($request->getContent('workingTime'), true);
        $workingTime = $workingTime['working_time'];
        if (empty($workingTime) || empty($workingTime['start'] ||empty($workingTime['end']))) {
            $view = $this->view('Missing argument', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        if (!$user) {
            $view = $this->view('User does not exist', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $newWorkingTime = new Workingtimes();
        $start = new \DateTime($workingTime['start']);
        $end = new \DateTime($workingTime['end']);
        $newWorkingTime->setStart($start)->setEnd($end);
        $user->addWorkingtime($newWorkingTime);
        $em->persist($newWorkingTime);
        $em->flush();
        $view = $this->view(['WorkingTimes created', $newWorkingTime], Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * Get workingTime by user.
     *
     * This is allowed to all
     *
     * @Rest\Get("/workingtimes/{userId}", name="get_workingtime")
     * @Rest\View()
     */
    public function get_workingtimeById(Request $request, $userId)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($userId);
        if (!$user) {
            $view = $this->view('User does not exist', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $view = $this->view(['workingTimes' => $user->getWorkingtimes()], Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * Get workingTime by user and date.
     *
     * This is allowed to all
     *
     * @Rest\Get("/workingtimes/{id}/{date}", name="get_working_time_by_id_and_date")
     * @Rest\View()
     */
    public function getWorkingTimeByIdAndDate(Request $request, $id, $date)
    {
        try {
            $date = new \DateTime($date);
        } catch (\Exception $e) {
            $view = $this->view('Date not valid', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($id);
        if (!$user) {
            $view = $this->view('User does not exist', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $results = $em->getRepository(Workingtimes::class)->SearchWorkingTimeByUserAndDate($user, $date);
        $view = $this->view(['WorkingTimes' => $results], Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * Delete a WorkingTime by its ID.
     *
     * This is allowed to all
     *
     * @Rest\Delete("/workingtimes/{id}", name="delete_workingtime")
     * @Rest\View()
     */
    public function delete_workingtimeById(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $workingTime = $em->getRepository(Workingtimes::class)->find($id);
        if (!$workingTime) {
            $view = $this->view('workingTime does not exist', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $em->remove($workingTime);
        $em->flush();
        $view = $this->view(['workingTime deleted'], Response::HTTP_OK);
        return $this->handleView($view);
    }

}