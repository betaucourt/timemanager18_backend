<?php

namespace App\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Manager\UserManager;

use FOS\RestBundle\Request\ParamFetcherInterface;
/**
 * User controller.
 *
 * @Route("/api")
 */
class UserController extends FOSRestController
{


    public $_Manager;

    public function __construct(UserManager $manager)
    {
        $this->_Manager = $manager;
    }

    /**
     * Create a new user.
     *
     * This is allowed to all
     * @Rest\Post("/users", name="new_user")
     * @Rest\View()
     */
    public function newUser(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $newUser = new User();
        $user = json_decode($request->getContent('user'), true);
        if (empty($user['user']) || empty($user['user']['username'])|| empty($user['user']['email']) || empty($user['user']['password'])) {
            $view = $this->view('Missing argument', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $newUser->setUsername($user['user']['username'])->setEmail($user['user']['email']);
        $newUser->setPlainPassword($user['user']['password']); $newUser->setEnabled(true);

        $test = $em->getRepository(User::class)->findOneBy(['email' => $newUser->getEmail()]);
        if ($test != null) {
            $view = $this->view('Email already exist', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $test = $em->getRepository(User::class)->findOneBy(['username' => $newUser->getUsername()
        ]);
        if ($test != null) {
            $view = $this->view('Username already exist', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $em->persist($newUser);
        $em->flush();
        $view = $this->view(['User Created', $newUser], Response::HTTP_OK);
        return $this->handleView($view);

    }

    /**
     * Returns information about who is the user.
     *
     *
     * @Rest\Get("/users", name="get_user")
     * @Rest\View()
     *
     */
    public function getUsers(Request $request)
    {
        $username = $request->query->get('username');
        $em = $this->getDoctrine()->getManager();
        $email = $request->query->get('email');
        if (!$username && !$email) {
            $users = $em->getRepository(User::class)->findAll();
            $view = $this->view(['Users', $users], Response::HTTP_OK);
            return $this->handleView($view);
        }
        $test = $em->getRepository(User::class)->findOneBy(['email' => $email, 'username' => $username]);
        if ($test == null) {
            $view = $this->view('User does not exist', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $view = $this->view(['User', $test], Response::HTTP_OK);
        return $this->handleView($view);

    }

    /**
     * Returns information about who is the user.
     *
     *
     * @Rest\Post("/login", name="get_login")
     * @Rest\View()
     *
     */
    public function getLogin(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || empty($data['user'])) {
            $view = $this->view('Missing datas', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        if (empty($data['user']['password'])) {
            $view = $this->view('Missing password', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        if (empty($data['user']['email'])) {
            $view = $this->view('Missing password', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $user = $this->_Manager->getUserByEmailAndPassword($data['user']['email'], $data['user']['password']);
        if (!$user) {
            $view = $this->view('User does not exist', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $user->setToken($this->_Manager->generateToken($user));
        $view = $this->view(['User' =>$user], Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * Returns information about who is the user.
     *
     *
     * @Rest\Get("/me", name="get_user_by_token")
     * @Rest\View()
     *
     */
    public function getUserByToken(Request $request)
    {
        if (($token = $request->headers->get('Authorization')) == null) {
            $view = $this->view('Invalid token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $user = $this->_Manager->getUserByToken($token);
        $view = $this->view(['User:', $user], Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * Returns information about who is the user.
     *
     *
     * @Rest\Get("/users/{id}", name="get_user_id")
     * @Rest\View()
     *
     */
    public function getUserById(Request $request, $id)
    {
        $password = json_decode($request->getContent(), true);
        if (empty($password) || empty($password['user'])) {
            $view = $this->view('Invalid password', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        if (empty($password['user']['password'])) {
            $view = $this->view('Invalid password', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        if (($user = $this->_Manager->checkUserByIdAndPassword($password['user']['password'], $id)) == null) {
            $view = $this->view('User does not exist', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['id' => $id]);
        $user->setToken($this->_Manager->generateToken($user));
        $view = $this->view(['User:', $user], Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * Modify user
     * This is allowed to any fully authenticated user
     *
     * @Rest\Put("/users/{id}", name="modify_user")
     * @Rest\View()
     */
    public function grantManager(Request $request)
    {

    }

    /**
     * Modify user
     * This is allowed to any fully authenticated user
     *
     * @Rest\Put("/users/{id}", name="modify_user")
     * @Rest\View()
     */
    public function modifyUser(request $request, ParamFetcherInterface $paramFetcher, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $userBody = json_decode($request->getContent('user'), true);
        if (empty($userBody['user'])) {
            $view = $this->view('Missing argument', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        if (($user = $this->_Manager->checkUserById($id)) == null) {
            $view = $this->view('User does not exist', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $name = $userBody['user']['username'];
        $email = $userBody['user']['email'];
        if (!$name || $name == "") {
            $nameMsg = "Didn't change";
        } else {
            $user->setUserName($name);
            $nameMsg = "changed";
        }
        if (!$email || $email == "") {
            $emailMsg = "Didn't change";
        } else {
            $user->setEmail($email);
            $emailMsg = "changed";
        }
        $em->flush();
        $view = $this->view(['Name' => $nameMsg, 'Email' => $emailMsg], Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * Get user's project
     * This is allowed to any fully authenticated user
     * 
     * @Rest\Delete("/users/{id}", name="user_project")
     * @Rest\View()
     */
    public function DeleteUser(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        if (($user = $this->_Manager->checkUserById($id)) == null) {
            $view = $this->view('User does not exist', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $em->remove($user);
        $em->flush();
        $view = $this->view(['User deleted'], Response::HTTP_OK);
        return $this->handleView($view);
    }

}