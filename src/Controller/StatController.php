<?php

namespace App\Controller;

use App\Entity\Clocks;
use App\Entity\Weeks;
use App\Manager\UserManager;
use DateTime;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;

use FOS\RestBundle\Request\ParamFetcherInterface;
/**
 * User controller.
 *
 * @Route("/api")
 */
class StatController extends FOSRestController
{
    public $_Manager;

    public function __construct(UserManager $manager)
    {
        $this->_Manager = $manager;
    }

    /**
     * Create a new Clock for a user.
     *
     * This is allowed to all
     *
     * @Rest\Get("/stats/day", name="Get_hours")
     * @Rest\View()
     */
    public function getHours(Request $request)
    {
        if (($token = $request->headers->get('Authorization')) == null) {
            $view = $this->view('Invalid Token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $user = $this->_Manager->getUserByToken($token);
        if (empty($user)) {
            $view = $this->view('Invalid Token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $today = new \DateTime('now');
        $values = $this->_Manager->getHoursByDay($user, $today);
        if ($values == null) {
            $view = $this->view('No data', Response::HTTP_OK);
            return $this->handleView($view);
        }
        $week = new Weeks($values, $today);
        $view = $this->view([
            'userId' => $user->getId(),
            'day' => $week],
            Response::HTTP_OK
        );
        return $this->handleView($view);
    }


    /**
     * Create a new Clock for a user.
     *
     * This is allowed to all
     *
     * @Rest\Get("/stats/week", name="Get_week")
     * @Rest\View()
     */
    public function getWeek(Request $request)
    {
        if (($token = $request->headers->get('Authorization')) == null) {
            $view = $this->view('Invalid Token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $user = $this->_Manager->getUserByToken($token);
        if (empty($user)) {
            $view = $this->view('Invalid Token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $weeks = [];
        $days = [];
        $weeks = [];
        $monday = new \DateTime();
        $monday->setTimestamp(strtotime('sunday last week'));
        for($i = 0;  $i != 7; $i++) {
            $monday->modify('+1 day');
            $weeks[] = new Weeks($this->_Manager->getHoursByDay($user, $monday), $monday->format('Y-m-d'));

        }
        $totalHours = 0;
        foreach ($weeks as $week) {
            $totalHours += $week->number;
        }
        $view = $this->view([
            'userId' => $user->getId(),
            'totalHours' => $totalHours,
            'week' => $weeks],
            Response::HTTP_OK
        );
        return  $this->handleView($view);
    }

    /**
     * Create a new Clock for a user.
     *
     * This is allowed to all
     *
     * @Rest\Get("/stats/week/{id}", name="Get_week_user")
     * @Rest\View()
     */
    public function getWeekUser(Request $request, $id)
    {
        if (($token = $request->headers->get('Authorization')) == null) {
            $view = $this->view('Invalid Token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $user = $this->_Manager->getUserByToken($token);
        if (empty($user)) {
            $view = $this->view('Invalid Token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        unset($user);
        $user = $this->_Manager->_em->getRepository(User::class)->find($id);//= $this->_Manager->getUserByToken($token);
        if (empty($user) || $user == null) {
            $view = $this->view('Invalid Token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $weeks = [];
        $days = [];
        $weeks = [];
        $monday = new \DateTime();
        $monday->setTimestamp(strtotime('sunday last week'));
        for($i = 0;  $i != 7; $i++) {
            $monday->modify('+1 day');
            $weeks[] = new Weeks($this->_Manager->getHoursByDay($user, $monday), $monday->format('Y-m-d'));

        }
        $totalHours = 0;
        foreach ($weeks as $week) {
            $totalHours += $week->number;
        }
        $view = $this->view([
            'userId' => $user->getId(),
            'totalHours' => $totalHours,
            'week' => $weeks],
            Response::HTTP_OK
        );
        return  $this->handleView($view);
    }

}