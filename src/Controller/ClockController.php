<?php

namespace App\Controller;

use App\Entity\Clocks;
use DateTime;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Form\UserType;

use FOS\RestBundle\Request\ParamFetcherInterface;
/**
 * User controller.
 *
 * @Route("/api")
 */
class ClockController extends FOSRestController
{
    /**
     * Create a new Clock for a user.
     *
     * This is allowed to all
     *
     * @Rest\Post("/clocks/{userId}", name="new_clock")
     * @Rest\View()
     */
    public function newClock(Request $request, $userId)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($userId);
        if (!$user) {
            $view = $this->view('User does not exist', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $clock = json_decode($request->getContent('clocks'), true);
        if (empty($clock['clock']) || empty($clock['clock']['status']) ||empty($clock['clock']['time'])) {
            $view = $this->view('Missing argument', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $clock = $clock['clock'];
        $newClock = new Clocks();
        $newClock->setTime(new \DateTime($clock['time']))->setStatus($clock['status']);
        $user->addClock($newClock);
        $em->persist($newClock);
        $em->flush();
        $view = $this->view(['Clock created', $newClock], Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * get Clocks for a user.
     *
     * This is allowed to all
     * @Rest\Get("/clocks/{userId}", name="get_clock")
     * @Rest\View()
     */
    public function getClockByUserId(Request $request, $userId)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($userId);
        if (!$user) {
            $view = $this->view('User does not exist', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $view = $this->view(['clocks' => $user->getClocks()], Response::HTTP_OK);
        return $this->handleView($view);
    }
}