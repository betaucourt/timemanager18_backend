<?php

namespace App\Controller;

use App\Entity\Clocks;
use App\Entity\Teams;
use DateTime;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Form\UserType;
use App\Manager\UserManager;
use FOS\RestBundle\Request\ParamFetcherInterface;
/**
 * User controller.
 *
 * @Route("/api")
 */
class TeamController extends FOSRestController
{

    public $_Manager;

    public function __construct(UserManager $manager)
    {
        $this->_Manager = $manager;
    }

    /**
     * Create a new Clock for a user.
     *
     * This is allowed to all
     *
     * @Rest\Post("/team", name="new_team")
     * @Rest\View()
     */
    public function newTeam(Request $request)
    {
        if (($token = $request->headers->get('Authorization')) == null) {
            $view = $this->view('Invalid token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $data =  $data = json_decode($request->getContent(), true);
        if (empty($data) || empty($data['team'])) {
            $view = $this->view('Invalid team', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        if (empty($data['team']['name'])) {
            $view = $this->view('Invalid name', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $user = $this->_Manager->getUserByToken($token);
        if (empty($user)) {
            $view = $this->view('ACHTUNG', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $team = $this->_Manager->createTeam($data['team']['name'], $data['team']['users'], $user);
        $view = $this->view(['Team' => $team], Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * Create a new Clock for a user.
     *
     * This is allowed to all
     *
     * @Rest\get("/team", name="get_teams")
     * @Rest\View()
     */
    public function getTeams(Request $request)
    {
        if (($token = $request->headers->get('Authorization')) == null) {
            $view = $this->view('Invalid Token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $user = $this->_Manager->getUserByToken($token);
        if (empty($user)) {
            $view = $this->view('Invalid Token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $teams = $user->getTeams();
        $admins = $user->getAdmins();
        if ($user->getRole() == "user") {
            $view = $this->view(['Team' => $teams], Response::HTTP_OK);
            return $this->handleView($view);
        }
        $second_return = $this->getDoctrine()->getManager(Teams::class)->findAll();
        $view = $this->view(['Team' => $second_return], Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * Create a new Clock for a user.
     *
     * This is allowed to all
     *
     * @Rest\get("/team/{id}", name="get_teams_user")
     * @Rest\View()
     */
    public function getTeams_user(Request $request, $id)
    {
        if (($token = $request->headers->get('Authorization')) == null) {
            $view = $this->view('Invalid Token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $user = $this->_Manager->_em->getRepository(User::class)->find($id);//= $this->_Manager->getUserByToken($token);
        if (empty($user) || $user == null) {
            $view = $this->view('Invalid user', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $teams = $user->getTeams();
        $view = $this->view(['Team' => $teams], Response::HTTP_OK);
        return $this->handleView($view);

    }


    /**
     * Create a new Clock for a user.
     *
     * This is allowed to all
     *
     * @Rest\get("/team/{teamId}", name="get_team")
     * @Rest\View()
     */
    public function getTeam(Request $request, $teamId)
    {
        if (($token = $request->headers->get('Authorization')) == null) {
            $view = $this->view('Invalid Token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $user = $this->_Manager->getUserByToken($token);
        if (empty($user)) {
            $view = $this->view('Invalid Token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $team = $this->_Manager->getTeamById($teamId);
        if ($team == null) {
            $view = $this->view('ACHTUNG', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $view = $this->view(['Team' => $team], Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * Create a new Clock for a user.
     *
     * This is allowed to all
     *
     * @Rest\Delete("/team/{teamId}", name="delete_team")
     * @Rest\View()
     */
    public function deleteTeam(Request $request, $teamId)
    {
        if (($token = $request->headers->get('Authorization')) == null) {
            $view = $this->view('Invalid Token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $user = $this->_Manager->getUserByToken($token);
        if (empty($user)) {
            $view = $this->view('Invalid Token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $team = $this->_Manager->getTeamById($teamId);
        if ($team == null) {
            $view = $this->view('ACHTUNG', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        if (!$this->_Manager->checkAdmin($team, $user)) {
            $view = $this->view('Not team Admin', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $this->_Manager->deleteTeam($team);
        $view = $this->view('Team deleted', Response::HTTP_BAD_REQUEST);
        return $this->handleView($view);
    }

    /**
     * Create a new Clock for a user.
     *
     * This is allowed to all
     *
     * @Rest\Put("/team/{teamId}", name="modify_team")
     * @Rest\View()
     */
    public function modifyAdmin(Request $request, $teamId)
    {
        if (($token = $request->headers->get('Authorization')) == null) {
            $view = $this->view('Invalid Token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $user = $this->_Manager->getUserByToken($token);
        if (empty($user)) {
            $view = $this->view('Invalid Token', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $team = $this->_Manager->getTeamById($teamId);
        if ($team == null) {
            $view = $this->view('ACHTUNG', Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        $team = $this->_Manager->modifyAdmin($request->getContent(), $teamId);
        $view = $this->view(['Team modified' => $team], Response::HTTP_BAD_REQUEST);
        return $this->handleView($view);
    }


}