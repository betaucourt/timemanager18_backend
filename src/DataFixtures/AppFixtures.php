<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Workingtimes;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraints\Date;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $em)
    {
        $admin = new User();
        $admin->setPlainPassword("admin");
        $admin->setEmail("admin@admin.com");
        $admin->setEnabled(true);
        $admin->setUsername("admin");
        $admin->setRole("admin");
        $em->persist($admin);

        $user1 = new User();
        $user1->setPlainPassword("user1");
        $user1->setEmail("user1@user1.com");
        $user1->setEnabled(true);
        $user1->setUsername("user1");
        $user1->setRole("user1");
        $em->persist($user1);

        $user2 = new User();
        $user2->setPlainPassword("user2");
        $user2->setEmail("user2@user2.com");
        $user2->setEnabled(true);
        $user2->setUsername("user2");
        $user2->setRole("user");
        $em->persist($user2);

        $user3 = new User();
        $user3->setPlainPassword("user3");
        $user3->setEmail("user3@user3.com");
        $user3->setEnabled(true);
        $user3->setUsername("user3");
        $user3->setRole("user");
        $em->persist($user3);

        $em->flush();
        $workingTimes = new Workingtimes();
        $monday = new \DateTime();
        $monday->setTimestamp(strtotime('sunday last week'));
        $hour = 7;$minute=11;$second=34;
        for($i = 0; $i !=6; $i++) {
            $newWorkingTime = new Workingtimes();
            $monday = $monday->modify('+1 day');

            $tmp = clone $monday;
            $tmp->setTime(7, 11, 34);
            $newWorkingTime->setStart($tmp);
            $em->persist($newWorkingTime);
            unset($tmp);

            $tmp1 = clone $monday;
            $tmp1 = $tmp1->setTime(15, 39, 34);
            $newWorkingTime->setEnd($tmp1);
            $em->persist($newWorkingTime);
            unset($tmp1);

            $admin->addWorkingtime($newWorkingTime);
            $em->persist($newWorkingTime);
            $em->flush();
            unset($tmp);
            unset($newWorkingTime);
            //$newWorkingTime->setStart($monday)
        }
    }
}
