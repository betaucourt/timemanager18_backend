<?php


namespace App\Manager;


use App\Entity\Teams;
use App\Entity\User;
use App\Entity\Workingtimes;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserManager
{

    public $_em;
    public $_jwtManager;
    public $_encoder;

    public function __construct(EntityManagerInterface $em, JWTTokenManagerInterface $JWTManager,
                                UserPasswordEncoderInterface $encoder)
    {
        $this->_em = $em;
        $this->_jwtManager = $JWTManager;
        $this->_encoder = $encoder;
    }

    public function checkUserById($userId)
    {
        $user = $this->_em->getRepository(User::class)->findOneBy(['id' => $userId]);
        if (!$user) {
            return null;
        }
        return $user;
    }

    public function createTeam($name, $users, User $user)
    {
        $newTeam = new Teams();
        $users = $this->_em->getRepository(User::class)->findUsersByEmail($users);
        $newTeam->setName($name);
        foreach ($users as $tmp) {
            $newTeam->addUser($tmp);
            $tmp->addTeam($newTeam);
        }
        $newTeam->addUser($user);
        $user->addTeam($newTeam);
        $newTeam->addAdmin($user->getEmail());

        $this->_em->persist($newTeam);
        $this->_em->flush();
        $user->addAdmin($newTeam->getId());
        $user->setRole("manager");
        $this->_em->flush();


        return $newTeam;
    }

    public function getTeamById($id)
    {
        $team = $this->_em->getRepository(Teams::class)->find($id);
        return $team;
    }

    public function checkAdmin(Teams $team, User $user)
    {
        if (array_search($user->getEmail(), $team->getAdmins()) !== false) {
            return true;
        }
        return false;
    }

    public function checkUserByIdAndPassword($password, $id)
    {
        $user = $this->_em->getRepository(User::class)->findOneBy(['id' => $id]);
        if (!$user || !$this->_encoder->isPasswordValid($user, $password)) {
            return null;
        }
        return $user;
    }

    public function deleteTeam(Teams $team)
    {

        $this->_em->remove($team);
        $this->_em->flush();
        return true;
    }

    public function getUserByEmailAndPassword($email, $password)
    {
        $user = $this->_em->getRepository(User::class)->findOneBy(['email' => $email]);
        if (!$user || !$this->_encoder->isPasswordValid($user, $password)) {
            return null;
        }
        return $user;
    }

    public function getUserByToken($token)
    {
        $tokenParts = explode(".", $token);
        $tokenHeader = base64_decode($tokenParts[0]);
        $tokenPayload = base64_decode($tokenParts[1]);
        $jwtHeader = json_decode($tokenHeader);
        $jwtPayload = json_decode($tokenPayload);
        return $this->_em->getRepository(User::class)->findOneBy(['username' => $jwtPayload->username]);
    }

    public function generateToken(User $user)
    {
        $jwt = $this->_jwtManager->create($user);
        return $jwt;
    }


    public function modifyAdmin($json, $id)
    {
        $json = json_decode($json, true);
        $team = null;
        if (empty($json || empty($json['team']))) {
            return $team;
        }
        $team = $this->_em->getRepository(Teams::class)->find($id);
        if (!empty($json['team']['admin'])) {
            $team->removeAdmin();
            $admins = $this->_em->getRepository(User::class)->findBy(['email' => $json['team']['admin']]);
            foreach ($admins as $admin) {
                $team->addAdmin($admin);
            }
        }

        if (!empty($json['team']['name'])) {
            $team->setName($json['team']['name']);
        }

        if (!empty($json['team']['users'])) {
            $team->cleanUsers();
            $users = $this->_em->getRepository(User::class)->findBy(['email' => $json['team']['users']]);
            foreach ($users as $user) {
                $team->addUser($user);
            }
            if (!empty($admins))
                foreach($admins as $admin) {
                    $team->addUser($admin);
                }
        }
        $this->_em->persist($team);
        $this->_em->flush();
        return $team;
    }

    public function getHoursByDay(User $user, \DateTime $actualDate)
    {
        $dates = $this->_em->getRepository(Workingtimes::class)->SearchWorkingTimeByUserAndDate($user, $actualDate);
        if ($dates == null) {
            return 0;
        }
        $intervals = [];
        foreach ($dates as $date) {
            $intervals []= $date->getStart()->diff($date->getEnd());
        }
        $hours = 0;
        $minutes = 0;
        foreach ($intervals as $interval) {
            $minutes += $interval->i;
            if ($minutes >= 60) {
                $minutes -= 60; $interval++;
            }
            $hours += $interval->h;
        }
        $toReturn = $hours;
        if ($minutes >= 30)
            $toReturn++;
        return $toReturn;
    }

    public function checkPrivileges($token, $role)
    {
        $user = $this->getUserByToken($token);
        if ($user == null) {
            return null;
        }
        if ($user->getRole() == "admin") {
            return $user;
        }
        if ($user->getRole() == "manager" && $role != "admin")
            return $user;
        if ($user->getRole() == $role)
            return $user;
    }
}