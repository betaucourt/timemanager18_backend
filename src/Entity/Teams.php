<?php

namespace App\Entity;

use App\Repository\TeamsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;

use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\SerializedName;
/**
 * @ORM\Entity(repositoryClass=TeamsRepository::class)
 * @JMS\ExclusionPolicy("ALL")
 * @ORM\Table(name="team")
 */
class Teams
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @JMS\Groups({"whoami"})
     * @JMS\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="Teams")
     * @JMS\Groups({"whoami"})
     * @JMS\Expose()
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Groups({"whoami"})
     * @JMS\Expose()
     */
    private $name;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Groups({"whoami"})
     * @JMS\Expose()
     */
    private $admin;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }

        return $this;
    }

    public function cleanUsers()
    {
        $this->users =  new ArrayCollection();;
    }


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAdmins()
    {
        return explode(',', $this->admin);
    }

    public function addAdmin($admin) {
        if (!$this->admin) {
            $this->admin = $admin;
            return $this->admin;
        }
        $admins = explode(',', $this->admin);
        array_push($admins, $admin);
        $this->admin = implode(",", $admins);
        return $this->admin;
    }

    public function setAdmins($admin) {
        $this->admin = $admin;
        return $this->admin;
    }

    public function removeAdmin() {
        $this->admin = null;
    }
}
