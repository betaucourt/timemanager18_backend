<?php


namespace App\Entity;


class Weeks
{

    public function __construct($interval, $date)
    {
        $this->number = $interval;
        $this->date = $date;
    }

    public $date;
    public $number;
}