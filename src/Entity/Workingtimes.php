<?php

namespace App\Entity;

use App\Repository\WorkingtimesRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=WorkingtimesRepository::class)
 */
class Workingtimes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $start;

    /**
     * @ORM\Column(type="datetime")
     */
    private $end;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $time;

    public function getTime()
    {
        return $this->time;
    }

    public function setTime(\DateTime $time)
    {
        $this->time = $time;
        return $this->time;
    }

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isNight;

    public function getIsNight()
    {
        return $this->isNight;
    }

    public function setIsNight(\DateTime $isNight)
    {
        $this->time = $isNight;
        return $this->isNight;
    }

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $eatcount;

    public function getEatcount()
    {
        return $this->eatcount;
    }

    public function setEatcount(\DateTime $eatcount)
    {
        $this->time = $eatcount;
        return $this->eatcount;
    }

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="workingtimes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(\DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(\DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
