<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;

use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\SerializedName;

define("USER", "user");
define("MANAGER", "manager");
define("ADMIN", "admin");

/**
 * @ORM\Entity
 * @ORM\Table(name="`user`")
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @JMS\ExclusionPolicy("ALL")
 */
class User extends BaseUser
{


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @JMS\Groups({"whoami", "get_project", "get_tickets"})
     * @JMS\Expose()
     */
    protected $id;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     *
     */
    protected $plainPassword;

    /**
     * @SerializedName("token")
     * @JMS\Groups({"fillable", "whoami"})
     * @JMS\Expose()
     */
    private $token;

    public function setToken($token)
    {
        $this->token = $token;
        return $token;
    }

    public function getToken()
    {
        return $this->token;
    }

    /**
     * @ORM\OneToMany(targetEntity=Workingtimes::class, mappedBy="user", orphanRemoval=true)
     * @SerializedName("workingTimes")
     * @JMS\Groups({"fillable", "whoami"})
     * @JMS\Expose()
     */
    private $workingtimes;

    /**
     * @ORM\OneToMany(targetEntity=Clocks::class, mappedBy="user", orphanRemoval=true)
     * @SerializedName("clocks")
     * @JMS\Groups({"fillable", "whoami"})
     * @JMS\Expose()
     */
    private $clocks;

    /**
     * @ORM\ManyToMany(targetEntity=Teams::class, inversedBy="teams")
     *
     * @JMS\Groups({"fillable", "whoami"})
     * @SerializedName("teams")
     * @JMS\Expose()
     */
    private $teams;

    /**
     *
     * @JMS\Groups({"fillable", "whoami"})
     * @SerializedName("admin")
     * @JMS\Expose()
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $admin;



    /**
     * @ORM\Column(type="string", length=255, options={"default": USER})
     *
     * @SerializedName("roles")
     * @JMS\Groups({"fillable", "whoami"})
     * @JMS\Expose()
     */
    private $role = USER;

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
        return $this->role;
    }

    public function __construct()
    {
        $this->clocks = new ArrayCollection();
        $this->workingtimes = new ArrayCollection();
        $this->teams = new ArrayCollection();
    }

    /**
     * @return Collection|Clocks[]
     */
    public function getClocks(): Collection
    {
        return $this->clocks;
    }

    public function addClock(Clocks $clock): self
    {
        if (!$this->clocks->contains($clock)) {
            $this->clocks[] = $clock;
            $clock->setUser($this);
        }

        return $this;
    }

    public function removeClock(Clocks $clock): self
    {
        if ($this->clocks->contains($clock)) {
            $this->clocks->removeElement($clock);
            // set the owning side to null (unless already changed)
            if ($clock->getUser() === $this) {
                $clock->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Workingtimes[]
     */
    public function getWorkingtimes(): Collection
    {
        return $this->workingtimes;
    }

    public function addWorkingtime(Workingtimes $workingtime): self
    {
        if (!$this->workingtimes->contains($workingtime)) {
            $this->workingtimes[] = $workingtime;
            $workingtime->setUser($this);
        }

        return $this;
    }

    public function removeWorkingtime(Workingtimes $workingtime): self
    {
        if ($this->workingtimes->contains($workingtime)) {
            $this->workingtimes->removeElement($workingtime);
            // set the owning side to null (unless already changed)
            if ($workingtime->getUser() === $this) {
                $workingtime->setUser(null);
            }
        }

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Teams[]
     */
    public function getTeams(): Collection
    {
        return $this->teams;
    }

    public function addTeam(Teams $team): self
    {
        if (!$this->teams->contains($team)) {
            $this->teams[] = $team;
        }

        return $this;
    }

    public function removeTeam(Teams $team): self
    {
        if ($this->teams->contains($team)) {
            $this->teams->removeElement($team);
        }

        return $this;
    }

    public function getAdmins()
    {
        return explode(',', $this->admin);
    }

    public function addAdmin($admin) {
        if (!$this->admin) {
            $this->admin = $admin;
            return $this->admin;
        }
        $admins = explode(',', $this->admin);
        array_push($admins, $admin);
        $this->admin = implode(",", $admins);
        return $this->admin;
    }

    public function setAdmins(string $admin) {
        $this->admin = $admin;
        return $this;
    }
}
