<?php

namespace App\Repository;

use App\Entity\Clocks;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Clocks|null find($id, $lockMode = null, $lockVersion = null)
 * @method Clocks|null findOneBy(array $criteria, array $orderBy = null)
 * @method Clocks[]    findAll()
 * @method Clocks[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClocksRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Clocks::class);
    }

    // /**
    //  * @return Clocks[] Returns an array of Clocks objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Clocks
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
