<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Workingtimes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Workingtimes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Workingtimes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Workingtimes[]    findAll()
 * @method Workingtimes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkingtimesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Workingtimes::class);
    }

    public function SearchWorkingTimeByUserAndDate(User $user, \DateTime $date)
    {
        $list = $this->createQueryBuilder('w')
            ->where('w.user = :id')
            ->setParameter('id', $user->getId())
            ->getQuery()->getResult();
        $results = [];
        foreach($list as $element) {
            if ($element->getStart()->format('Y-m-d') == $date->format('Y-m-d')) {
                array_push($results, $element);
            }
        }
        return $results;
    }


    // /**
    //  * @return Workingtimes[] Returns an array of Workingtimes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Workingtimes
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
